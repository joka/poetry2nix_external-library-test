{
  description = "Application packaged using poetry2nix";

  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  inputs.poetry2nix = {
    url = "github:nix-community/poetry2nix";
    inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, flake-utils, poetry2nix }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        inherit (poetry2nix.legacyPackages.${system}) mkPoetryApplication mkPoetryPackages mkPoetryEnv;
        pkgs = nixpkgs.legacyPackages.${system};
      in
      {
        packages = {
          poetry-app = mkPoetryApplication { projectDir = self; preferWheels = true; };
          poetry-env = (mkPoetryEnv { projectDir = self; preferWheels = true; });
          default = self.packages.${system}.poetry-app;
        };
      });
}
